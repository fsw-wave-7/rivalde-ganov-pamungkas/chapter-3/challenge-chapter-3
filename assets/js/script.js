// DOM SELECTOR & MANIPULATION
const body = document.getElementsByTagName('body')[0];
body.style.backgroundColor = "#9C835F";


function pilihanComputer() {
    let comp = Math.random();
    if (comp < 0.34) return 'batu';
    if (comp >= 0.34 && comp < 0.67) return 'kertas';
    return 'gunting';
}

function hasil(player, comp) {
    // rules game
    if (player == comp) return 'DRAW';
    if (player == 'batu') return (comp == 'gunting') ? 'PLAYER 1 WIN' : 'COM WIN';
    if (player == 'gunting') return (comp == 'kertas') ? 'PLAYER 1 WIN' : 'COM WIN';
    if (player == 'kertas') return (comp == 'batu') ? 'PLAYER 1 WIN' : 'COM WIN';
}


const imgCompB = document.querySelector('#com img.batu')
const imgCompK = document.querySelector('#com img.kertas')
const imgCompG = document.querySelector('#com img.gunting')
const vs = document.querySelector('.vs')

const pilihan = document.querySelectorAll('#player ul li img');
pilihan.forEach(function (pil) {
    pil.addEventListener('click', function () {
        const pilihanComp = pilihanComputer();
        const pilihanPlayer = pil.classList.item(1);
        const hasilSuit = hasil(pilihanPlayer, pilihanComp);
        console.log(` player : ${pilihanPlayer}`)
        console.log(` comp : ${pilihanComp}`)
        console.log(` HASIL : ${hasilSuit}`)

        vs.innerHTML = hasilSuit;
        vs.classList.add('info')

        if (pilihanComp == 'batu') return imgCompB.classList.add('class', 'pilih') || imgCompK.classList.remove('pilih') || imgCompG.classList.remove('pilih')
        if (pilihanComp == 'kertas') return imgCompK.classList.add('class', 'pilih') || imgCompB.classList.remove('pilih') || imgCompG.classList.remove('pilih')
        if (pilihanComp == 'gunting') return imgCompG.classList.add('class', 'pilih') || imgCompB.classList.remove('pilih') || imgCompK.classList.remove('pilih')

    })
})

// refresh
function reset() {
    imgCompB.classList.remove('pilih')
    imgCompK.classList.remove('pilih')
    imgCompG.classList.remove('pilih')
    vs.classList.remove('info')
    vs.innerHTML = 'VS'
}

const refresh = document.getElementById('refresh');
refresh.onclick = reset;