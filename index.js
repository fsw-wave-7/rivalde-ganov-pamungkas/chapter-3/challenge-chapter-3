const express = require('express');
const app = express();
// const morgan = require('morgan');

app.set('view engine','ejs')

// app.use(morgan('dev'))
app.get(express.json());
app.get(express.urlencoded({ extended: false }));
// const router = require('./router');
// app.get('/iniError', (req, res) => {
//     iniError // Ini penyebab error!
// })
// app.use(router);

// Internal Server Error Handlers
app.use(function(err, req, res, next) {
    console.error(err);
    res.status(500).json({
        status: 'fail',
        errors: err.message
    })
})

app.use(express.static('public'))

//404 Handler
app.use(function(err, req, res, next) {
    res.status(404).json({
        status: 'fail',
        errors: 'Are you lost?'
    })
})

// const logger = (req, res, next) => {
//     console.log(`${req.method} ${req.url}`);
// }

// app.use(logger)

app.get('/', (req, res) => {
    res.render('pages/index');
})

app.get('/game', (req, res) => {
    res.render('pages/game')
})

app.get('/daftar', (req, res) => {
    res.json([
     {
        id: 1,
        Nama: Agung,
        user_id: 1
     },
     {
        id: 2,
        Nama: Dedek,
        user_id: 1
     },
    ])
})

// app.get('/endpointLain', (req, res) => {
//     res.send("Lain")
// })

app.listen(3000, () => {
    console.log("Server Berjalan!");
})